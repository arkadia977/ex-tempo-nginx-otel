# FROM --platform=amd64 nginx:1.23.1
FROM --platform=amd64 nginx:1.25.3

# Define the search path for shared libraries used when compiling and running NGINX
ENV LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/opt/opentelemetry-webserver-sdk/sdk_lib/lib

COPY opentelemetry-webserver-sdk /opt/opentelemetry-webserver-sdk

RUN apt-get update \
  && apt-get install -y --no-install-recommends dumb-init unzip \
  && /opt/opentelemetry-webserver-sdk/install.sh \
  && echo "load_module /opt/opentelemetry-webserver-sdk/WebServerModule/Nginx/1.25.3/ngx_http_opentelemetry_module.so;\n$(cat /etc/nginx/nginx.conf)" > /etc/nginx/nginx.conf

COPY opentelemetry_module.conf /etc/nginx/conf.d/opentelemetry_module.conf

EXPOSE 80

STOPSIGNAL SIGQUIT
